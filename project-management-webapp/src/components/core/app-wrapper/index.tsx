import React, { FunctionComponent } from 'react';
import styled from "styled-components";

const AppWrapper = styled.div`
  display: flex;
  flex-grow : 1;
  flex-direction: row;
  width : 100%;
  height : 100%;
  justify-content: flex-start;
  align-items : stretch ;
  background-color: #f2f4f4;
`;

interface AppWrapperProp {
    children: JSX.Element[] | JSX.Element
}
  
const AppWrapperComponent: FunctionComponent<AppWrapperProp> = ({ children }) => <AppWrapper>{children}</AppWrapper>

export default AppWrapperComponent;