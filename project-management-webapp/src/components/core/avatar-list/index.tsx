import React from 'react';
import styled from "styled-components";
import Avatar from "../avatar";
import IconButton from '../button-icon';
import Badge from '../badge';
import { ReactComponent as PlusIcon } from '../../../assets/add.svg';

const AvatarListWrapper = styled.div<AvatarListProp>`
  display: flex;
  flex-direction: row ;
  align-items:center;
  justify-content:center;
`;

interface AvatarListProp {
  number?: number,
  onClick?: (e?: React.MouseEvent) => void
}

interface IAvatar {
  name: String,
  url?: String
}

const avatars: Array<IAvatar> = [{ name: "coucou", url: "test" }, { name: "coucou", url: "test" }, { name: "coucou", url: "test" }]

const AvatarList = ({ onClick, number = 2 }: AvatarListProp) => (
  <AvatarListWrapper onClick={onClick}>
    {avatars.map((items, index) => {
      if (index <= number)
        return (<Avatar size="small" />)
      if (index === (number + 1))
        return (
          <IconButton size="small">
            <span>{`+${avatars.length - number - 1}`}</span>
          </IconButton>
        )
    }
    )}
  </AvatarListWrapper>)

export default AvatarList;