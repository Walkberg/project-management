import React from 'react';
import styled from "styled-components";
import { ReactComponent as ReactLogo } from '../../../assets/dashboard.svg';

const AvatarWrapper = styled.div<AvatarWrapperProp>`
  display: flex;
  flex-direction: column ;
  align-items:center;
  justify-content:center;
  width : ${(props: AvatarWrapperProp) => props.size ? props.size.toString() : 25}px;
  height : ${(props: AvatarWrapperProp) => props.size ? props.size.toString() : 25}px;
  margin :5px 0px 5px 0px;
  padding :10px;
  border-radius : 100%;
  cursor:pointer;
  background-color : #ffcc1b;
  div:hover {
    background-color: red;
  }
`;

interface AvatarWrapperProp {
  size: Number,
  clickable?: boolean,
  onClick?: (e?: React.MouseEvent) => void
}

interface AvatarProp {
  size: String,
  clickable?: boolean,
  onClick?: (e?: React.MouseEvent) => void
}

const getSize = (size: String) => {
  switch (size) {
    case "small":
      return 15;
    default: return 25;
  }
}

const Avatar = ({ onClick, clickable = true, size }: AvatarProp) => (
  <AvatarWrapper size={getSize(size)} onClick={onClick}>
    <ReactLogo color="white" width="20px" height="20px" />
  </AvatarWrapper>)

export default Avatar;