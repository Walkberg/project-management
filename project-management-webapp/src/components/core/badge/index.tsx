import React, { FunctionComponent } from 'react';
import styled from "styled-components";

const BadgeWrapper = styled.span`
  display: inline-flex;
  position: relative;
  flex-direction: column ;
  align-items:center;
`;

const Badge = styled.span`
  position:absolute;
  top: 0;
  right: 0;
  background-color:red;
  color:white;
  height:20px;
  font-size: 0.75rem;
  padding: 0 6px;
  display:flex;
  align-items: center;
  align-content :center;
  border-radius:100%;
  transform: scale(1) translate(50%, -50%);
  transform-origin: 100% 0%;
`;


interface SideBarWrapperProp {
  children?: JSX.Element[] | JSX.Element
}

const SidebarWrapperComponent: FunctionComponent<SideBarWrapperProp> = ({ children }) =>
  <BadgeWrapper>
    {children}
    <Badge> 4 </Badge>
  </BadgeWrapper>

export default SidebarWrapperComponent;