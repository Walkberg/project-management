import React from 'react';
import styled from "styled-components";
import { ReactComponent as ReactLogo } from '../../../assets/add.svg';

const ButtonIcon = styled.div<AvatarWrapperProp>`
  display: flex;
  flex-direction: column ;
  align-items:center;
  justify-content:center;
  width : ${(props: AvatarWrapperProp) => props.size ? props.size.toString() : 25}px;
  height : ${(props: AvatarWrapperProp) => props.size ? props.size.toString() : 25}px;
  margin :5px 0px 5px 0px;
  padding :10px;
  border-radius : 100%;
  border: 1px dotted;
  color :grey
  &:hover { backgroundColor :grey}
`;

interface AvatarWrapperProp {
  size: Number,
  onClick?: (e?: React.MouseEvent) => void
}

interface AvatarProp {
  size: String,
  children?: JSX.Element[] | JSX.Element,
  onClick?: (e?: React.MouseEvent) => void
}

const getSize = (size: String) => {
  switch (size) {
    case "small":
      return 10;
    default: return 25;
  }
}

const IconButton = ({ onClick, size, children }: AvatarProp) => (
  <ButtonIcon size={getSize(size)} onClick={onClick}>
    {children}
  </ButtonIcon>)

export default IconButton;