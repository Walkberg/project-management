import React, { FunctionComponent } from 'react';
import styled from "styled-components";

const SidebarWrapper = styled.main`
  display: flex;
  flex-grow:1;
  border-radius : 50px  0px 0px 50px;
  flex-direction: column ;
  align-items:center;
  background-color: #ffffff;
`;

interface ContentWrapperProp {
  children?: JSX.Element[] | JSX.Element
}

const test = [{test :2},{test :2},{test :2}]

const ContentWrapperComponent: FunctionComponent<ContentWrapperProp> = ({ children }) => 
  <SidebarWrapper>{test.map((item, index) => (<div key={index}>{item.test}</div>))}</SidebarWrapper>

export default ContentWrapperComponent;