import React from 'react';
import styled from "styled-components";

const SidebarContainer = styled.div`
  display: flex;
  flex-direction: row ;
  width :100%;
  align-items:center;
  justify-content: space-between;
  height : 50px;
  background-color:green;
`;

const NavbarFilter = () => {
  
  return (
  <SidebarContainer>
    filtre
  </SidebarContainer>
  )
}

export default NavbarFilter;