import React, { FunctionComponent, useState } from 'react';
import styled from "styled-components";
import { ReactComponent as ReactLogo } from '../../../assets/dashboard.svg';

const SidebarContainer = styled.div`
  display: flex;
  flex-direction: row ;
  width :100%;
  align-items:center;
  justify-content: space-between;
  margin-left :5px;
  margin-right :5px;
  height : 50px;
  color:black;
`;

const IconWrapper = styled.button`
  justify-content: center;
  align-items:center;
`;

const NavbarTitle = () => {
  return (
    <SidebarContainer>
      Projects
      <IconWrapper>
        <ReactLogo width="20px" height="20px" />
      </IconWrapper>
    </SidebarContainer>
  )
}

export default NavbarTitle;