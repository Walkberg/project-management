import React from 'react';
import styled from "styled-components";

const NavbarWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items:center;
  min-width : 200px;
  padding-left :25px;
  padding-right :25px;
`;

export default NavbarWrapper;