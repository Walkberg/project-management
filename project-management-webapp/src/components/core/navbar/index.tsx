import React, { useState } from 'react';
import styled from "styled-components";
import NavbarWrapper from "../navbar-wrapper"
import NavbarTitle from "../navbar-title"
import NavbarFilter from "../navbar-filter"
import ProjectCard from "../project-card"

const SidebarContainer = styled.div`
  display: flex;
  flex-direction: column ;
  align-items:center;
  width:100%;
`;

interface IItem { test: Number, url?: String, icon?: Node }

const sidebarItem: Array<IItem> = [{ test: 2 }, { test: 2 }, { test: 2 }]

const Sidebar = () => {

  const [selected, setSelected] = useState<Number>(2);

  const handleClick = (index: Number) => setSelected(index);

  return (
    <NavbarWrapper>
      <NavbarTitle />
      <NavbarFilter />
      <SidebarContainer>
        {sidebarItem.map((item, index) => (<ProjectCard onClick={() => handleClick(index)} selected={selected === index} />))}
      </SidebarContainer>
    </NavbarWrapper>)
}

export default Sidebar;