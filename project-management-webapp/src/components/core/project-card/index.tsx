import React from 'react';
import styled from "styled-components";
import { ReactComponent as ReactLogo } from '../../../assets/dashboard.svg';
import AvatarList from "../avatar-list";
import IconButton from '../button-icon';
import { ReactComponent as PlusIcon } from '../../../assets/add.svg';

const ProjectCardWrapper = styled.div<SideBarProp>`
  display: flex;
  flex-direction: column;
  align-items:center;
  margin :5px 0px 5px 0px;
  border-radius : 8px;
  background-color : white;
  box-shadow: 6px 6px 2px 1px rgba(0, 0, 255, .2);
`;

const IconWrapper = styled.button`
  display: flex;
  flex-grow : 1;
  align-items:center;
  margin :10px;
`;

const TitleWrapper = styled.div`
  display: flex;
  flex-grow : 1;
  align-items:center;
  color:black;
`;

const CustomerWrapper = styled.div`
  display: flex;
  flex-grow : 1;
  align-items:center;
  color:black;
`;

const TaskWrapper = styled.div`
  display: flex;
  flex-grow : 1;
  align-items:center;
  color:black;
`;

const Chips = styled.button`
display: flex;
flex-grow : 1;
align-items:center;
margin :10px;
color:black;
`;

interface SideBarProp {
  selected: Boolean,
  Icon?: Node,
  onClick: (e?: React.MouseEvent) => void
}

const SidebarItem = ({ selected, onClick, Icon: icon }: SideBarProp) => (
  <ProjectCardWrapper selected={selected} onClick={onClick}>
    <TitleWrapper>
      coucou
      <IconWrapper>
        <ReactLogo height="50px" width="50px" color="green" />
      </IconWrapper>
    </TitleWrapper>
    <CustomerWrapper>
      for customer
    </CustomerWrapper>
    <TaskWrapper>
      <Chips>In progress</Chips>
      <AvatarList />
      <IconButton size="small">
        <PlusIcon />
      </IconButton>
    </TaskWrapper>
  </ProjectCardWrapper>)

export default SidebarItem;