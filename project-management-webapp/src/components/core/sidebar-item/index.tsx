import React, { FunctionComponent } from 'react';
import styled from "styled-components";
import { ReactComponent as ReactLogo } from '../../../assets/dashboard.svg';

const SideBarItemWrapper = styled.div<SideBarProp>`
  display: flex;
  flex-direction: column ;
  align-items:center;
  width : 20px;
  height : 20px;
  margin :5px 0px 5px 0px;
  padding :10px;
  border-radius : 8px;
  cursor:pointer;
  background-color :${(props: SideBarProp) => (props.selected && 'black')};
  color:${(props: SideBarProp) => (props.selected ? 'white' : '#999')};;
`;

interface SideBarProp {
  selected: Boolean,
  Icon?: Node,
  onClick: (e?: React.MouseEvent) => void
}

const SidebarItem = ({ selected, onClick, Icon: icon }: SideBarProp) => (
  <SideBarItemWrapper selected={selected} onClick={onClick}>
    <ReactLogo />
  </SideBarItemWrapper>)

export default SidebarItem;