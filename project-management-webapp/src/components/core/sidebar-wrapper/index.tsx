import React, { FunctionComponent } from 'react';
import styled from "styled-components";

const SidebarWrapper = styled.div`
  display: flex;
  flex-direction: column ;
  align-items:center;
  min-width : 80px;
`;

interface SideBarWrapperProp {
  children?: JSX.Element[] | JSX.Element
}

const SidebarWrapperComponent: FunctionComponent<SideBarWrapperProp> = ({ children }) => 
  <SidebarWrapper>{children}</SidebarWrapper>

export default SidebarWrapperComponent;