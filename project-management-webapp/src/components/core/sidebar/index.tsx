import React, { FunctionComponent, useState } from 'react';
import styled from "styled-components";
import SideBarWrapper from "../sidebar-wrapper"
import SidebarItem from "../sidebar-item"
import Avatar from "../avatar"
import { ReactComponent as ReactLogo } from '../../../assets/dashboard.svg';

const SidebarContainer = styled.div`
  display: flex;
  flex-direction: column ;
  align-items:center;
  min-width : 80px;
`;

const Grow = styled.div`
flex-grow: 1;
`;

const LogoWrapper = styled.div`
margin-top: 10px;
`;

interface IItem {test :Number, url?:String, icon?:Node}

const sidebarItem : Array<IItem> = [{test :2},{test :2},{test :2},{test :2},{test :2},{test :2},{test :2}]

const Sidebar = () => {
  const [selected, setSelected] = useState<Number>(2);

  const handleClick = (index :Number) => setSelected(index);

  return (<SideBarWrapper>
   <>
      <LogoWrapper>
        <ReactLogo color="black" width="30px" height="30px"/>
      </LogoWrapper>
      <SidebarContainer>
        {sidebarItem.map((item, index) => (<SidebarItem onClick={() => handleClick(index)} selected={selected === index}/>))}
      </SidebarContainer>
      <Grow/>
      <Avatar size="medium"/>
    </>
  </SideBarWrapper>)}

export default Sidebar;