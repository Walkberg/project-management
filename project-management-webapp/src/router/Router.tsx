import React from 'react';
import AppWrapper from "../components/core/app-wrapper"
import Sidebar from "../components/core/sidebar"
import NavbarWrapper from "../components/core/navbar-wrapper"
import ContentWrapper from "../components/core/content-wrapper"
import NavBar from "../components/core/navbar"

function App() {
  return (<AppWrapper>
      <Sidebar/>
      <NavBar/>
      <ContentWrapper></ContentWrapper>
      </AppWrapper>)
}

export default App;